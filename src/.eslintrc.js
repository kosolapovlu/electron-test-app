module.exports = {
  root: true,
  extends: "react-app",
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint', 'react'],
  env: {
    browser: true,
  },
  rules: {
    semi: 0,
    quotes: 0,
    'linebreak-style': 0,
    indent: 0,
    'no-useless-escape': 0,
    'function-paren-newline': 'warn',
  },
}

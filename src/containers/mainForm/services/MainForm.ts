import {MainFormState} from '../../../store/mainForm/types/MainFormState'
import {dataPrivileges} from '../data/dataPrivileges'
import {dataButtons} from '../data/dataButtons'
import {Privilege} from '../../../store/mainForm/types/Privilege'
import {dataPrivilegeToItems} from '../data/dataPrivilegeToItems'
import {dataMarks} from "../data/dataMarks";

class MainForm {
  private _state: MainFormState
  private _mainFormActions: any

  constructor(state: MainFormState, mainFormActions: any) {
    this._state = state
    this._mainFormActions = mainFormActions
  }

  private static getDefaultPrivilege(privileges: Privilege[]): Privilege | undefined {
    return privileges.find((p) => p.password === '')
  }

  public loadData(): void {
    const privileges: Privilege[] = dataPrivileges
    const defaultPrivilege: Privilege | undefined = MainForm.getDefaultPrivilege(privileges)
    this._mainFormActions.setLoadData(
      privileges,
      dataButtons,
      dataMarks,
      defaultPrivilege,
      dataPrivilegeToItems,
    )
  }

  handleChangePassword(password: string): void {
    this._mainFormActions.setPassword(password)
    this._mainFormActions.setError('')
  }

  handleChangeIsShowModal(): void {
    this._mainFormActions.setIsShowModal()
  }

  handleSubmit(): void {
    const {privileges, password} = this._state

    let privilege: Privilege | undefined
    if (password) {
      privilege = privileges.find((p) => p.password === password)

      if (!privilege) {
        this._mainFormActions.setError('Вы ввели неверный пароль')
        this._mainFormActions.setPrivilege(MainForm.getDefaultPrivilege(privileges))
      } else {
        this._mainFormActions.setPrivilege(privilege)
      }
    }
  }
}

export {MainForm}

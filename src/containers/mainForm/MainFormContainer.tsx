import React, {useEffect, useState} from 'react'
import {MainFormComponent} from './MainFormComponent'
import {connectComponent} from '../../store/utils/connectComponent'
import {State} from '../../store/types/State'
import {MainFormState} from '../../store/mainForm/types/MainFormState'
import {mainFormActions} from '../../store/mainForm'
import {MainForm} from './services/MainForm'

interface MainFormProps {
  mainFormState: MainFormState
  mainFormActions: any
}

const MainFormContainer: React.FC<MainFormProps> = ({
  mainFormState,
  mainFormActions,
}): React.ReactElement => {
  const mainForm: MainForm = new MainForm(mainFormState, mainFormActions)

  useEffect(() => {
    mainForm.loadData()
  }, [])

  return <MainFormComponent mainFormState={mainFormState} mainForm={mainForm} />
}

export default connectComponent(
  (state: State) => ({
    mainFormState: state.mainForm,
  }),
  (dispatch: any) => ({
    mainFormActions: mainFormActions(dispatch),
  }),
  MainFormContainer,
)

import {Privilege} from '../../../store/mainForm/types/Privilege'
import {PrivilegeType} from '../../../store/mainForm/types/PrivilegeType'

export const dataPrivileges: Privilege[] = [
  {
    password: '',
    type: PrivilegeType.OPERATOR,
  },
  {
    password: '111',
    type: PrivilegeType.TECHNIC,
  },
  {
    password: '222',
    type: PrivilegeType.ENGINEER,
  },
]

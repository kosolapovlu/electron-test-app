import {FormButton} from '../../../store/mainForm/types/FormButton'

export const dataButtons: FormButton[] = [
  {
    id: '1',
    name: 'Кнопка 1',
    onClick: () => {
      console.log('Кнопка 1')
    },
  },
  {
    id: '2',
    name: 'Кнопка 2',
    onClick: () => {
      console.log('Кнопка 2')
    },
  },
  {
    id: '3',
    name: 'Кнопка 3',
    onClick: () => {
      console.log('Кнопка 3')
    },
  },
  {
    id: '4',
    name: 'Кнопка 4',
    onClick: () => {
      console.log('Кнопка 4')
    },
  },
  {
    id: '5',
    name: 'Кнопка 5',
    onClick: () => {
      console.log('Кнопка 5')
    },
  },
  {
    id: '6',
    name: 'Кнопка 6',
    onClick: () => {
      console.log('Кнопка 6')
    },
  },
]

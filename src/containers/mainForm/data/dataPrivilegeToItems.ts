import {PrivilegeToItemsMap} from '../../../store/mainForm/types/PrivilegeToItemsMap'

export const dataPrivilegeToItems: PrivilegeToItemsMap = {
  OPERATOR: ['1', '2'],
  TECHNIC: ['1', '2', '3', '4'],
  ENGINEER: ['1', '2', '3', '4', '5', '6'],
}

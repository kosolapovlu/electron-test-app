import {FormButton} from '../../../store/mainForm/types/FormButton'
import {FormMark} from '../../../store/mainForm/types/FormMark'

export const dataMarks: FormMark[] = [
  {id: '1', text: 'Текст 1'},
  {id: '2', text: 'Текст 2'},
  {id: '3', text: 'Текст 3'},
  {id: '4', text: 'Текст 4'},
  {id: '5', text: 'Текст 5'},
  {id: '6', text: 'Текст 6'},
]

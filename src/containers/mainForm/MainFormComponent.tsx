import React, {memo, ReactNode} from 'react'
import {Button} from '../../componets/Button'
import {Input} from '../../componets/Input'
import {MainFormState} from '../../store/mainForm/types/MainFormState'
import {MainForm} from './services/MainForm'
import {FormFeedback} from '../../componets/FormFeedback'
import {FormButton} from '../../store/mainForm/types/FormButton'
import {Modal} from '../../componets/Modal'
import {FormMark} from '../../store/mainForm/types/FormMark'
import {Mark} from '../../componets/Mark'

interface MainFormComponentProps {
  mainFormState: MainFormState
  mainForm: MainForm
}

const MainFormComponent: React.FC<MainFormComponentProps> = memo(
  ({mainFormState, mainForm}): React.ReactElement => {
    const {
      privileges,
      buttons,
      password,
      error,
      privilege,
      privilegeToItems,
      isShowModal,
      marks,
    } = mainFormState

    const renderPasswordBlock = (): ReactNode => {
      return (
        <div className={'password-block'}>
          <Input
            className={'mb-primary'}
            type={'text'}
            name={'password'}
            id={'password'}
            value={password}
            onChange={(value) => {
              mainForm.handleChangePassword(value)
            }}
          />
          {error && (
            <FormFeedback className={'mb-secondary'} type={'failure'}>
              {error}
            </FormFeedback>
          )}
          <Button
            id={'button-submit'}
            onClick={() => {
              mainForm.handleSubmit()
            }}>
            Войти
          </Button>
        </div>
      )
    }

    const renderItems = (itemType: 'button' | 'mark'): ReactNode => {
      let items: ReactNode[] = []
      if (privilege && privilegeToItems[privilege.type]) {
        for (let itemId of privilegeToItems[privilege.type]) {
          switch (itemType) {
            case 'button':
              {
                const button: FormButton | undefined = buttons.find((b) => b.id === itemId)
                if (button) {
                  items.push(
                    <Button
                      id={button.id}
                      onClick={button.onClick}
                      className={'mb-secondary'}
                      key={button.id}>
                      {button.name}
                    </Button>,
                  )
                }
              }
              break
            case 'mark':
              {
                const mark: FormMark | undefined = marks.find((m) => m.id === itemId)
                if (mark) {
                  items.push(<Mark text={mark.text} key={mark.id} />)
                }
              }
              break
          }
        }
      }
      return <div className={'buttons-block'}>{items}</div>
    }

    const renderModalBlock = (): ReactNode => {
      return (
        <div>
          <Button
            id={'open-modal'}
            onClick={() => {
              mainForm.handleChangeIsShowModal()
            }}>
            Модальное окно
          </Button>
          <Modal
            name={'Модальное окно'}
            isOpen={isShowModal}
            onClose={() => {
              mainForm.handleChangeIsShowModal()
            }}>
            {renderItems('mark')}
          </Modal>
        </div>
      )
    }

    const renderContent = (): ReactNode => {
      if (privileges.length !== 0) {
        return (
          <>
            {renderPasswordBlock()}
            {renderItems('button')}
            {renderModalBlock()}
          </>
        )
      } else {
        return (
          <div className={'loading'}>
            <p>Пожалуйста подождите, идет загрузка данных</p>
          </div>
        )
      }
    }

    return <div className={'main-form'}>{renderContent()} </div>
  },
)

export {MainFormComponent}

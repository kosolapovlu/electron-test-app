import {connect, MapStateToPropsParam} from 'react-redux'

function connectComponent(
  mapStateToProps: MapStateToPropsParam<any, any, any>,
  actions: any,
  component: React.FC<any>,
): any {
  return connect(mapStateToProps, actions, null, {pure: true})(component)
}

export {connectComponent}

import {BaseAction} from '../types/BaseAction'
import {MainFormTypes} from './types'
import {bindActionCreators} from 'redux'
import {Privilege} from './types/Privilege'
import {FormButton} from './types/FormButton'
import {PrivilegeToItemsMap} from './types/PrivilegeToItemsMap'
import {FormMark} from './types/FormMark'

export const actions = {
  setError: (error: string): BaseAction => ({
    type: MainFormTypes.SET_ERROR,
    payload: error,
  }),

  setPassword: (password: string): BaseAction => ({
    type: MainFormTypes.SET_PASSWORD,
    payload: password,
  }),

  setPrivilege: (privilege: Privilege | undefined) => ({
    type: MainFormTypes.SET_PRIVILEGE,
    payload: privilege,
  }),

  setLoadData: (
    privileges: Privilege[],
    buttons: FormButton[],
    marks: FormMark[],
    privilege: Privilege | undefined,
    privilegeToItems: PrivilegeToItemsMap,
  ) => ({
    type: MainFormTypes.SET_LOAD_DATA,
    payload: {privileges, buttons, marks, privilege, privilegeToItems},
  }),

  setIsShowModal: () => ({
    type: MainFormTypes.SET_IS_SHOW_MODAL,
  }),
}

export function mainFormActions(dispatch: any) {
  return bindActionCreators(actions, dispatch)
}

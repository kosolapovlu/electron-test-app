import {actions, mainFormActions} from './actions'
import mainForm from './reducers'
import {MainFormTypes} from './types'

export {actions, MainFormTypes, mainForm, mainFormActions}

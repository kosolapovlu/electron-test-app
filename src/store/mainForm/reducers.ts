import {MainFormState} from './types/MainFormState'
import {BaseAction} from '../types/BaseAction'
import {MainFormTypes} from './types'

export const INITIAL_STATE: MainFormState = {
  error: '',
  password: '',
  privilege: undefined,
  buttons: [],
  marks: [],
  privileges: [],
  privilegeToItems: {},
  isShowModal: false,
}

export default function mainForm(
  state: MainFormState = INITIAL_STATE,
  action: BaseAction,
): MainFormState {
  switch (action.type) {
    case MainFormTypes.SET_ERROR: {
      return {
        ...state,
        error: action.payload,
      }
    }

    case MainFormTypes.SET_PASSWORD: {
      return {
        ...state,
        password: action.payload,
      }
    }

    case MainFormTypes.SET_PRIVILEGE: {
      return {
        ...state,
        privilege: action.payload,
      }
    }

    case MainFormTypes.SET_LOAD_DATA: {
      return {
        ...state,
        privileges: action.payload.privileges,
        buttons: action.payload.buttons,
        marks: action.payload.marks,
        privilege: action.payload.privilege,
        privilegeToItems: action.payload.privilegeToItems,
      }
    }

    case MainFormTypes.SET_IS_SHOW_MODAL: {
      return {
        ...state,
        isShowModal: !state.isShowModal,
      }
    }

    default: {
      return {
        ...state,
      }
    }
  }
}

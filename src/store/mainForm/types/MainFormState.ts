import {FormButton} from './FormButton'
import {Privilege} from './Privilege'
import {PrivilegeToItemsMap} from './PrivilegeToItemsMap'
import {FormMark} from './FormMark'

export interface MainFormState {
  password: string
  error: string
  privilege: Privilege | undefined
  privileges: Privilege[]
  buttons: FormButton[]
  marks: FormMark[]
  privilegeToItems: PrivilegeToItemsMap
  isShowModal: boolean
}

export interface FormButton {
  id: string
  name: string
  onClick: () => void
}

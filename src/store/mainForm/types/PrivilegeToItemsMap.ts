export interface PrivilegeToItemsMap {
  [privilege: string]: string[]
}

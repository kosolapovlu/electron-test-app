import {PrivilegeType} from "./PrivilegeType";

export interface Privilege {
  type: PrivilegeType
  password: string
}

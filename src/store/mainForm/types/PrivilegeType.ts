export enum PrivilegeType {
  OPERATOR = 'OPERATOR',
  TECHNIC = 'TECHNIC',
  ENGINEER = 'ENGINEER',
}

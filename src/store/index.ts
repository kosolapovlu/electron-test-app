import {createStore, combineReducers} from 'redux'
import mainForm from './mainForm/reducers'

const rootReducer = combineReducers({
  mainForm,
})

const store = createStore(rootReducer)

export default store

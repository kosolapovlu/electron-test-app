import {MainFormState} from '../mainForm/types/MainFormState'

export interface State {
  mainForm: MainFormState
}

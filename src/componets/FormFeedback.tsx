import React, {memo} from 'react'

interface FormFeedbackProps {
  type: 'success' | 'failure'
  className?: string
}

const FormFeedback: React.FC<FormFeedbackProps> = memo(
  ({type, className, children}): React.ReactElement => {
    return (
      <div className={className}>
        <p className={'form-feedback ' + type}>{children}</p>
      </div>
    )
  },
)

export {FormFeedback}

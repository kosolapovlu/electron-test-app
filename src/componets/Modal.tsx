import React from 'react'

interface ModalProps {
  isOpen: boolean
  onClose: () => void | Promise<void>
  name?: string
}

const Modal: React.FC<ModalProps> = ({isOpen, onClose, children, name}): React.ReactElement => {
  const onClickBackdrop = (evt: any) => {
    onClose()
  }

  const onClickContent = (evt: any) => {
    evt.stopPropagation()
  }

  return (
    <div className={'modal' + (isOpen ? ' open' : '')} onClick={onClickBackdrop}>
      <div className={'modal-content'} onClick={onClickContent}>
        {name && (
          <div className={'header'}>
            <p className={'name'}>{name}</p>
          </div>
        )}
        <div className={'body'}>{children}</div>
      </div>
    </div>
  )
}

export {Modal}

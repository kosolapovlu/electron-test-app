import React, {memo} from 'react'

interface MarkProps {
  text: string
  className?: string
}

const Mark: React.FC<MarkProps> = memo(
  ({text, className}): React.ReactElement => {
    return <p className={'mark ' + (className ?? className)}>{text}</p>
  },
)

export {Mark}

import React, {memo} from 'react'

interface InputProps {
  id: string
  type: 'text' | 'number'
  name: string
  value: any
  valid?: boolean
  feedback?: string
  required?: boolean
  disabled?: boolean
  onChange: (value: any) => any
  placeholder?: string
  min?: number
  max?: number
  step?: number
  className?: string
}

const Input: React.FC<InputProps> = memo(
  ({
    id,
    name,
    type,
    value,
    required,
    disabled,
    onChange,
    placeholder,
    min,
    max,
    step,
    className,
  }): React.ReactElement => {
    return (
      <div className={'input-container ' + (className ?? '')}>
        <input
          id={id}
          name={name}
          type={type}
          value={value}
          required={required}
          disabled={disabled}
          className={'input'}
          onChange={(evt) => {
            onChange(evt.target.value)
          }}
          placeholder={placeholder}
        />
      </div>
    )
  },
)

export {Input}

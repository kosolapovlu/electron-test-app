import React, {memo} from 'react'

interface ButtonProps {
  id: string
  onClick: () => void
  className?: string
  disabled?: boolean
}

const Button: React.FC<ButtonProps> = memo(
  ({id, onClick, className, disabled, children}): React.ReactElement => {
    return (
      <button
        id={id}
        type={'button'}
        onClick={onClick}
        className={'button ' + (className ?? '')}
        disabled={disabled}>
        {children}
      </button>
    )
  },
)

export {Button}

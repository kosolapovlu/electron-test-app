import React from 'react'
import './App.css'
import MainForm from './containers/mainForm/MainFormContainer'
import {Provider} from 'react-redux'
import store from './store'

function App() {
  return (
    <Provider store={store}>
      <div className="container">
        <MainForm />
      </div>
    </Provider>
  )
}

export default App
